using NUnit.Framework;
using Moq;
using HrDepartment.Application.Services;
using HrDepartment.Infrastructure.Interfaces;
using HrDepartment.Domain.Entities;
using System;
using HrDepartment.Domain.Enums;
using System.Threading.Tasks;
using HrDepartment.Application.Interfaces;

namespace HrDepartment.Application.Tests
{
    public class HrDepartmentApplicationTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(1, 20, EducationLevel.University, 10, BadHabits.None, false, 95)]
        [TestCase(2, 80, EducationLevel.University, 10, BadHabits.None, false, 85)]
        [TestCase(3, 20, EducationLevel.School, 0, BadHabits.Smoking, false, 25)]
        [TestCase(4, 50, EducationLevel.College, 5, BadHabits.Smoking, false, 55)]
        [TestCase(5, 50, EducationLevel.College, 5, BadHabits.Smoking, true, 0)]
        public void RateJobSeekerAsync_CalculateJobSeekerRatingAsync(int id, int age, EducationLevel education, int experience, BadHabits badHabits, bool isInSanctionsList, int expectedResult)
        {
            var jobSeeker = GetNewJobSeeker(id, age, education, experience, badHabits);
            var result = GetNewJobSeekerRateService(isInSanctionsList).CalculateJobSeekerRatingAsync(jobSeeker).Result;

            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(1, 20, EducationLevel.University, 10, BadHabits.None, false, 95)]
        [TestCase(2, 80, EducationLevel.University, 10, BadHabits.None, false, 85)]
        [TestCase(3, 20, EducationLevel.School, 0, BadHabits.Smoking, false, 25)]
        [TestCase(4, 50, EducationLevel.College, 5, BadHabits.Smoking, false, 55)]
        [TestCase(5, 50, EducationLevel.College, 5, BadHabits.Smoking, true, 0)]
        public void JobSeekerService_RateJobSeekerAsync(int id, int age, EducationLevel education, int experience, BadHabits badHabits, bool isInSanctionsList, int expectedResult)
        {
            var jobSeeker = GetNewJobSeeker(id, age, education, experience, badHabits);

            IStorage storage = Mock.Of<IStorage>(m => m.GetByIdAsync<JobSeeker, int>(id) == Task.Run(() => jobSeeker));
            INotificationService notificationService = Mock.Of<INotificationService>(m => m.NotifyRockStarFoundAsync(jobSeeker) == Task.Run(() => Console.WriteLine("NotifyRockStarFound") ));
            JobSeekerService jobSeekerService = new JobSeekerService(storage, GetNewJobSeekerRateService(isInSanctionsList), null, notificationService);

            var result = jobSeekerService.RateJobSeekerAsync(id).Result;

            Assert.AreEqual(expectedResult, result);
        }

        private static JobSeeker GetNewJobSeeker(int id, int age, EducationLevel education, int experience, BadHabits badHabits)
        {
            return new JobSeeker()
            {
                Id = id,
                LastName = "������" + id,
                FirstName = "����",
                MiddleName = "��������",
                BirthDate = new DateTime(DateTime.Now.Year - age, 1, 1),
                Education = education,
                Status = JobSeekerStatus.New,
                Experience = experience,
                BadHabits = badHabits
            };
        }

        private JobSeekerRateService GetNewJobSeekerRateService(bool isInSanctionsList)
        {
            SanctionService sanctionService = new SanctionService(isInSanctionsList);
            return new JobSeekerRateService(sanctionService);
        }

        private class SanctionService : ISanctionService
        {
            private bool _isInSanctionsList = false;

            public SanctionService(bool isInSanctionsList)
            {
                _isInSanctionsList = isInSanctionsList;
            }

            public Task<bool> IsInSanctionsListAsync(string lastName, string firstName, string middleName, DateTime birthDate)
            {
                return Task.Run(() => _isInSanctionsList);
            }
        }
    }
}